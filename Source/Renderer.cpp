#include "Renderer.h"
#include "GeometryNode.h"
#include "Tools.h"
#include "ShaderProgram.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "OBJLoader.h"

#include <math.h>

#include <algorithm>
#include <array>
#include <iostream>


#define _PI_ 3.14159f

// RENDERER
Renderer::Renderer()
{
	this->m_nodes = {};

	this->m_corridor_nodes = {};
	this->m_wall_nodes = {};

	this->m_continous_time = 0.0;
}

Renderer::~Renderer()
{
	glDeleteTextures(1, &m_fbo_depth_texture);
	glDeleteTextures(1, &m_fbo_pos_texture);
	glDeleteTextures(1, &m_fbo_normal_texture);
	glDeleteTextures(1, &m_fbo_albedo_texture);
	glDeleteTextures(1, &m_fbo_mask_texture);

	glDeleteFramebuffers(1, &m_fbo);

	glDeleteVertexArrays(1, &m_vao_fbo);
	glDeleteBuffers(1, &m_vbo_fbo_vertices);
}

bool Renderer::Init(int SCREEN_WIDTH, int SCREEN_HEIGHT)
{
	this->m_screen_width = SCREEN_WIDTH;
	this->m_screen_height = SCREEN_HEIGHT;

	bool techniques_initialization = InitShaders();

	bool meshes_initialization = InitGeometricMeshes();
	bool light_initialization = InitLights();

	bool common_initialization = InitCommonItems();
	bool inter_buffers_initialization = InitIntermediateBuffers();

	//If there was any errors
	if (Tools::CheckGLError() != GL_NO_ERROR)
	{
		printf("Exiting with error at Renderer::Init\n");
		return false;
	}

	this->BuildWorld();
	this->InitCamera();

	//If everything initialized
	return techniques_initialization && meshes_initialization &&
		common_initialization && inter_buffers_initialization;
}

void Renderer::BuildWorld()
{
	for (int i = 0; i < m_corridor_nodes.size(); i++) {
		GeometryNode& corridor = *this->m_corridor_nodes[i];

		corridor.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, i * corridor_length));
		
		//corridor.m_aabb.center = glm::vec3(corridor.model_matrix * glm::vec4(corridor.m_aabb.center, 1.f));	// original - not compatible with restart
		corridor.m_aabb.center = glm::vec3(0.f, 0.f, i * corridor_length - corridor_length / 2);
	}
	
	for (int i = 0; i < m_wall_nodes.size(); i++) {
		GeometryNode& wall = *this->m_wall_nodes[i];

		// Pick at random a lateral displacement distance (close or far) with a chance 70% for the first one to make it a bit harder
		float lateral_displacement = (rand() % 100 < 70) ? wall_lateral_displacement_close : wall_lateral_displacement_far;
		// Pick a direction for the displacement (left or right)
		lateral_displacement = (rand() % 100 < 50) ? lateral_displacement : lateral_displacement * (-1);


		//wall.model_matrix = glm::rotate(glm::mat4(1.f), _PI_ / 2, glm::vec3(0.f, 0.f, 1.f));
		wall.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(lateral_displacement, 0.f, clear_start_length + (i + 1) * default_wall_distance));

		wall.m_aabb.center = glm::vec3(lateral_displacement, 0.f, clear_start_length + (i + 1) * default_wall_distance);
	}

	// Place the collision hull exactly where the first wall is
	wall_collision_hull->model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(m_wall_nodes[0]->m_aabb.center.x, m_wall_nodes[0]->m_aabb.center.y, m_wall_nodes[0]->m_aabb.center.z));
	wall_collision_hull->m_aabb.center = m_wall_nodes[0]->m_aabb.center;



	this->m_world_matrix = glm::mat4(1.f);
}

void Renderer::InitCamera()
{
	this->m_camera_position = starting_position;
	this->m_camera_target_position = glm::vec3(starting_position.x, starting_position.y, 100);


	this->m_camera_up_vector = glm::vec3(0, 1, 0);

	this->m_view_matrix = glm::lookAt(
		this->m_camera_position,
		this->m_camera_target_position,
		m_camera_up_vector);

	this->m_projection_matrix = glm::perspective(
		glm::radians(45.f),
		this->m_screen_width / (float)this->m_screen_height,
		0.1f, 100.f);
}

bool Renderer::InitLights()
{
	this->m_light.SetColor(glm::vec3(initial_light_intensity));
	this->m_light.SetPosition(glm::vec3(starting_position.x, (floor_level + starting_position.y) / 2, starting_position.z));
	this->m_light.SetTarget(glm::vec3(0, floor_level, player_height * 2));
	this->m_light.SetConeSize(15, 15);
	return true;
}

bool Renderer::InitShaders()
{
	std::string vertex_shader_path = "Assets/Shaders/geometry pass.vert";
	std::string geometry_shader_path = "Assets/Shaders/geometry pass.geom";
	std::string fragment_shader_path = "Assets/Shaders/geometry pass.frag";

	m_geometry_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_geometry_program.LoadGeometryShaderFromFile(geometry_shader_path.c_str());
	m_geometry_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_geometry_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/deferred pass.vert";
	fragment_shader_path = "Assets/Shaders/deferred pass.frag";

	m_deferred_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_deferred_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_deferred_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/post_process.vert";
	fragment_shader_path = "Assets/Shaders/post_process.frag";

	m_post_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_post_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_post_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/shadow_map_rendering.vert";
	fragment_shader_path = "Assets/Shaders/shadow_map_rendering.frag";

	m_spot_light_shadow_map_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_spot_light_shadow_map_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_spot_light_shadow_map_program.CreateProgram();

	return true;
}

bool Renderer::InitIntermediateBuffers()
{
	glGenTextures(1, &m_fbo_depth_texture);
	glGenTextures(1, &m_fbo_pos_texture);
	glGenTextures(1, &m_fbo_normal_texture);
	glGenTextures(1, &m_fbo_albedo_texture);
	glGenTextures(1, &m_fbo_mask_texture);
	glGenTextures(1, &m_fbo_texture);

	glGenFramebuffers(1, &m_fbo);

	return ResizeBuffers(m_screen_width, m_screen_height);
}

bool Renderer::ResizeBuffers(int width, int height)
{
	m_screen_width = width;
	m_screen_height = height;

	// texture
	glBindTexture(GL_TEXTURE_2D, m_fbo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, m_screen_width, m_screen_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	// framebuffer to link to everything together
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_pos_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_fbo_normal_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_fbo_albedo_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_fbo_mask_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_fbo_depth_texture, 0);

	GLenum status = Tools::CheckFramebufferStatus(m_fbo);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		return false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

bool Renderer::InitCommonItems()
{
	glGenVertexArrays(1, &m_vao_fbo);
	glBindVertexArray(m_vao_fbo);

	GLfloat fbo_vertices[] = {
		-1, -1,
		1, -1,
		-1, 1,
		1, 1, };

	glGenBuffers(1, &m_vbo_fbo_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_fbo_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	return true;
}

bool Renderer::InitGeometricMeshes()
{
	std::array<const char*, OBJECTS::SIZE_ALL> assets = {
		"Assets/game_assets/Wall.obj",
		"Assets/game_assets/Corridor_Straight.obj",

		"Assets/game_assets/CH-Wall.obj"
	};

	bool initialized = true;
	OBJLoader loader;

	for (int32_t i = 0; i < CORRIDOR_COUNT; ++i)
	{
		auto& asset = assets[OBJECTS::CORRIDOR_STRAIGHT];

		GeometricMesh* mesh = loader.load(asset);

		if (mesh != nullptr)
		{
			GeometryNode* node = new GeometryNode();
			node->Init(mesh);
			this->m_corridor_nodes.push_back(node);
			delete mesh;
		}
		else
		{
			initialized = false;
		}
	}

	for (int32_t i = 0; i < WALL_COUNT; ++i)
	{
		auto& asset = assets[OBJECTS::WALL];

		GeometricMesh* mesh = loader.load(asset);

		if (mesh != nullptr)
		{
			CollidableNode* node = new CollidableNode();
			node->Init(mesh);
			this->m_wall_nodes.push_back(node);
			delete mesh;
		}
		else
		{
			initialized = false;
		}
	}


	GeometricMesh* mesh = loader.load(assets[OBJECTS::CH_WALL]);
	wall_collision_hull = new CollidableNode();
	wall_collision_hull->Init(mesh);
	delete mesh;


	return initialized;
}

void Renderer::Update(float dt)
{
	this->UpdateGeometry(dt);
	this->UpdateCamera(dt);
	m_continous_time += dt;
}

void Renderer::UpdateGeometry(float dt)
{
	
	//-- UPDATE CORRIDORS AND WALLS -----------------------------------------------------------------------------------------------------------

	// If the first corridor is behind the camera, move it after all the other corridors - both geometrically and in the vector
	GeometryNode& firstCorridor = *this->m_corridor_nodes[0];
	if (firstCorridor.m_aabb.center.z + corridor_length / 2 < m_camera_position.z) {

		// Translate the model matrix itself to move the corridor ahead of the rest
		firstCorridor.model_matrix = glm::translate(glm::mat4(1.f),
			glm::vec3(
				0.f, 
				0.f, 
				corridor_length * CORRIDOR_COUNT + firstCorridor.m_aabb.center.z + corridor_length / 2));
		firstCorridor.m_aabb.center += glm::vec3(0.f, 0.f, corridor_length * CORRIDOR_COUNT);

		// Push the updated corridor at the back of the vector
		m_corridor_nodes.erase(m_corridor_nodes.begin());
		m_corridor_nodes.push_back(&firstCorridor);
		
		// Alternatively
		//auto iterator = m_corridor_nodes.begin();
		//std::rotate(iterator, iterator + 1, m_corridor_nodes.end());

	}
	// Update the APP model matrix
	firstCorridor.app_model_matrix = firstCorridor.model_matrix;
	
	// Do the same for the walls
	GeometryNode& firstWall = *this->m_wall_nodes[0];
	if (firstWall.m_aabb.center.z + wall_thickness / 2 < m_camera_position.z) {

		// Pick at random a lateral displacement distance (close or far) with a chance 70% for the first one to make it a bit harder
		float lateral_displacement = (rand() % 100 < 70) ? wall_lateral_displacement_close : wall_lateral_displacement_far;
		// Pick a direction for the displacement (left or right)
		lateral_displacement = (rand() % 100 < 50) ? lateral_displacement : lateral_displacement * (-1);

		// Translate the model matrix itself to move the wall ahead of the rest AND give it a new lateral displacement - after negating the previous one
		firstWall.model_matrix = glm::translate(glm::mat4(1.f),
			glm::vec3(
				lateral_displacement, 
				0.f, 
				firstWall.m_aabb.center.z + default_wall_distance * WALL_COUNT));  // ABSOLUTE
		firstWall.m_aabb.center = glm::vec3(lateral_displacement, 0.f, firstWall.m_aabb.center.z + default_wall_distance * WALL_COUNT);

		// Push the updated wall at the back of the vector
		m_wall_nodes.erase(m_wall_nodes.begin());
		m_wall_nodes.push_back(&firstWall);

	}
	// Update the APP model matrix
	firstWall.app_model_matrix = firstWall.model_matrix;


	// Update the app matrices for walls and corridors only at the start of the program
	// From now on the code that translates the corridors or the walls will take care of this
	if (firstUpdate) {
		for (auto& node : m_corridor_nodes) {
			node->app_model_matrix = node->model_matrix;
		}
		
		for (auto& node : m_wall_nodes) {
			node->app_model_matrix = node->model_matrix;
		}

		firstUpdate = false;
	}
	

	//--- UPDATE WALL COLLISION HULL ----------------------------------------------------------------------------------------------------------
	wall_collision_hull->model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(m_wall_nodes[0]->m_aabb.center.x, m_wall_nodes[0]->m_aabb.center.y, m_wall_nodes[0]->m_aabb.center.z));
	wall_collision_hull->app_model_matrix = wall_collision_hull->model_matrix;
	wall_collision_hull->m_aabb.center = m_wall_nodes[0]->m_aabb.center;


	//--- UPDATE LIGHT ------------------------------------------------------------------------------------------------------------------------

	this->m_light.SetPosition(glm::vec3(
		m_camera_position.x, 
		(floor_level + m_camera_position.y) / 2, 
		m_camera_position.z));

	this->m_light.SetTarget(glm::vec3(
		0.f, 
		floor_level, 
		m_camera_position.z + player_height * 2));
	

	float current_intensity = m_light.GetColor().x;
	if (gameEnded && current_intensity > 0.f) {
		m_light.SetColor(glm::vec3( std::max(current_intensity - current_intensity * dt / 1.5f , 0.f) ));
	}

}

void Renderer::UpdateCamera(float dt)
{
	if (gameEnded)
		return;
	
	//--- CAMERA MOVEMENT SPEED --------------------------------------------------------------------------

	float standard_speed = glm::pi<float>() * 8.0f;
	float speed_multiplier = (0.9 + m_camera_position.z / 300.f / 10.f);
	float movement_speed = speed_multiplier * standard_speed;	// increasing speed

	if (currently_running)
		movement_speed *= running_speed_multiplier;


	//--- CAMERA MOVEMENT ON Z AXIS ----------------------------------------------------------------------
	
	glm::vec3 direction = glm::normalize(m_camera_target_position - m_camera_position);

	//m_camera_position = m_camera_position + (m_camera_movement.x * movement_speed * dt) * direction;
	//m_camera_target_position = m_camera_target_position + (m_camera_movement.x * movement_speed * dt) * direction;
	
	m_camera_position = m_camera_position + (movement_speed * dt) * direction;
	m_camera_target_position = m_camera_target_position + (movement_speed * dt) * direction;
	
	glm::vec3 right = glm::normalize(glm::cross(direction, m_camera_up_vector));


	//--- CAMERA MOVEMENT ON X AXIS ----------------------------------------------------------------------

	float target_position_X = lateral_movement_limit * (- m_camera_movement.y);		// can have three values, since m_camera_movement.y can be -1, 0, or 1
	int lateral_direction = 0;			// the lateral direction to follow: -1 for left, 1 for right, 0 for non
	if (m_camera_position.x - target_position_X > 0)
		lateral_direction = 1;
	else if (m_camera_position.x - target_position_X < 0)
		lateral_direction = -1;

	// if the camera is too close to the target x, just snap it there - otherwise, move it linearly
	if(abs(m_camera_position.x - target_position_X) < 0.1 * speed_multiplier)
		m_camera_position.x = target_position_X;
	else
		m_camera_position = m_camera_position + (lateral_direction * movement_speed * dt) * right;


	//--- CAMERA MOVEMENT ON Y AXIS ----------------------------------------------------------------------
	
	float camera_height_increment;
	if (InAir()) {		//(time_of_jump != -1 && time_of_jump + expected_jump_duration > m_continous_time) {
		float time_since_jump = m_continous_time - time_of_jump;
		
		camera_height_increment = launch_speed_of_jump * time_since_jump - 0.5 * g * pow(time_since_jump * jump_multiplier, 2);
	}
	else if (Sliding()) {
		float time_since_slide = m_continous_time - time_of_slide;
		// y = (2*u)^4 - (player_height - height_reduction_of_slide)	 with u = x - first_root
		float u = time_since_slide - first_root;
		camera_height_increment = pow(2*u, 4) - (player_height - height_reduction_of_slide);
	}
	else {
		camera_height_increment = 0;
	}

	m_camera_position = glm::vec3(
		m_camera_position.x, 
		starting_position.y + camera_height_increment, 
		m_camera_position.z);


	//--- CAMERA TARGET DIRECTION BASED ON Y AXIS MOVEMENT -----------------------------------------------

	float look_y = (Sliding()) ? 
		starting_position.y : 
		m_camera_position.y;
	
	m_camera_target_position = glm::vec3(
		m_camera_position.x,
		look_y,
		m_camera_position.z + 10.0);

	m_view_matrix = glm::lookAt(m_camera_position, m_camera_target_position, m_camera_up_vector);

}

bool Renderer::ReloadShaders()
{
	m_geometry_program.ReloadProgram();
	m_post_program.ReloadProgram();
	m_deferred_program.ReloadProgram();
	m_spot_light_shadow_map_program.ReloadProgram();
	return true;
}

void Renderer::Render()
{
	RenderShadowMaps();
	RenderGeometry();
	RenderDeferredShading();
	RenderPostProcess();

	GLenum error = Tools::CheckGLError();

	if (error != GL_NO_ERROR)
	{
		printf("Reanderer:Draw GL Error\n");
		system("pause");
	}
}

void Renderer::RenderPostProcess()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);

	m_post_program.Bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fbo_texture);
	m_post_program.loadInt("uniform_texture", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_light.GetShadowMapDepthTexture());
	m_post_program.loadInt("uniform_shadow_map", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	m_post_program.loadInt("uniform_tex_pos", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	m_post_program.loadInt("uniform_tex_normal", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	m_post_program.loadInt("uniform_tex_albedo", 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	m_post_program.loadInt("uniform_tex_mask", 5);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	m_post_program.loadInt("uniform_tex_depth", 6);

	glBindVertexArray(m_vao_fbo);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	m_post_program.Unbind();
}

void Renderer::RenderStaticGeometry()
{
	glm::mat4 proj = m_projection_matrix * m_view_matrix * m_world_matrix;

	RenderStaticGeometryForAll(proj, m_nodes);
	RenderStaticGeometryForAll(proj, m_corridor_nodes);
	RenderStaticGeometryForAll(proj, m_wall_nodes);
}

void Renderer::RenderStaticGeometryForAll(glm::mat4 proj, std::vector<GeometryNode*> nodes)
{
	for (auto& node : nodes)
	{
		glBindVertexArray(node->m_vao);

		m_geometry_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);
		m_geometry_program.loadMat4("uniform_normal_matrix", glm::transpose(glm::inverse(m_world_matrix * node->app_model_matrix)));
		m_geometry_program.loadMat4("uniform_world_matrix", m_world_matrix * node->app_model_matrix);

		for (int j = 0; j < node->parts.size(); ++j)
		{
			m_geometry_program.loadVec3("uniform_diffuse", node->parts[j].diffuse);
			m_geometry_program.loadVec3("uniform_ambient", node->parts[j].ambient);
			m_geometry_program.loadVec3("uniform_specular", node->parts[j].specular);
			m_geometry_program.loadFloat("uniform_shininess", node->parts[j].shininess);
			m_geometry_program.loadInt("uniform_has_tex_diffuse", (node->parts[j].diffuse_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_emissive", (node->parts[j].emissive_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_mask", (node->parts[j].mask_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_normal", (node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_is_tex_bumb", (node->parts[j].bump_textureID > 0) ? 1 : 0);


			// Blinn-Phong stuff
			m_geometry_program.loadVec3("uniform_light_color", m_light.GetColor());
			m_geometry_program.loadVec3("uniform_light_dir", m_light.GetDirection());
			m_geometry_program.loadVec3("uniform_light_pos", m_light.GetPosition());

			m_geometry_program.loadFloat("uniform_light_umbra", m_light.GetUmbra());
			m_geometry_program.loadFloat("uniform_light_penumbra", m_light.GetPenumbra());

			m_geometry_program.loadVec3("uniform_camera_pos", m_camera_position);
			m_geometry_program.loadVec3("uniform_camera_dir", normalize(m_camera_target_position - m_camera_position));
			



			glActiveTexture(GL_TEXTURE0);
			m_geometry_program.loadInt("uniform_tex_diffuse", 0);
			glBindTexture(GL_TEXTURE_2D, node->parts[j].diffuse_textureID);

			if (node->parts[j].mask_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE1);
				m_geometry_program.loadInt("uniform_tex_mask", 1);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].mask_textureID);
			}

			if ((node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0))
			{
				glActiveTexture(GL_TEXTURE2);
				m_geometry_program.loadInt("uniform_tex_normal", 2);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].bump_textureID > 0 ?
					node->parts[j].bump_textureID : node->parts[j].normal_textureID);
			}

			if (node->parts[j].emissive_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE3);
				m_geometry_program.loadInt("uniform_tex_emissive", 3);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].emissive_textureID);
			}

			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
		}

		glBindVertexArray(0);
	}
}

void Renderer::RenderCollidableGeometry()
{
	glm::mat4 proj = m_projection_matrix * m_view_matrix * m_world_matrix;

	glm::vec3 camera_dir = normalize(m_camera_target_position - m_camera_position);


	// Please don't judge
	std::vector<CollidableNode*> v = {};
	v.push_back(wall_collision_hull);
	RenderCollidableGeometryForAll(proj, v);
}

void Renderer::RenderCollidableGeometryForAll(glm::mat4 proj, std::vector<CollidableNode*> nodes) {

	for (auto& node : nodes)
	{
		float_t isectT = 0.f;
		int32_t primID = -1;
		int32_t totalRenderedPrims = 0;

		
		glBindVertexArray(node->m_vao);

		m_geometry_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);
		m_geometry_program.loadMat4("uniform_normal_matrix", glm::transpose(glm::inverse(m_world_matrix * node->app_model_matrix)));
		m_geometry_program.loadMat4("uniform_world_matrix", m_world_matrix * node->app_model_matrix);
		m_geometry_program.loadFloat("uniform_time", m_continous_time);

		for (int j = 0; j < node->parts.size(); ++j)
		{
			m_geometry_program.loadVec3("uniform_diffuse", node->parts[j].diffuse);
			m_geometry_program.loadVec3("uniform_ambient", node->parts[j].ambient);
			m_geometry_program.loadVec3("uniform_specular", node->parts[j].specular);
			m_geometry_program.loadFloat("uniform_shininess", node->parts[j].shininess);
			m_geometry_program.loadInt("uniform_has_tex_diffuse", (node->parts[j].diffuse_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_mask", (node->parts[j].mask_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_emissive", (node->parts[j].emissive_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_normal", (node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_is_tex_bumb", (node->parts[j].bump_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_prim_id", primID - totalRenderedPrims);

			glActiveTexture(GL_TEXTURE0);
			m_geometry_program.loadInt("uniform_tex_diffuse", 0);
			glBindTexture(GL_TEXTURE_2D, node->parts[j].diffuse_textureID);

			if (node->parts[j].mask_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE1);
				m_geometry_program.loadInt("uniform_tex_mask", 1);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].mask_textureID);
			}

			if ((node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0))
			{
				glActiveTexture(GL_TEXTURE2);
				m_geometry_program.loadInt("uniform_tex_normal", 2);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].bump_textureID > 0 ?
					node->parts[j].bump_textureID : node->parts[j].normal_textureID);
			}

			if (node->parts[j].emissive_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE3);
				m_geometry_program.loadInt("uniform_tex_emissive", 3);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].emissive_textureID);
			}

			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			totalRenderedPrims += node->parts[j].count;
		}

		glBindVertexArray(0);
	}

}

void Renderer::CollisionCheck() {

	float_t isectT = 0.f;
	int32_t primID = -1;
	int32_t totalRenderedPrims = 0;

	// Check whether the current collidable is close enough to the camera on the Z axis (since that's the camera's movement axis)
	// If it is, draw a ray a bit ahead of the camera to check whether they actually are about to collide
	if (!gameEnded &&
		abs(wall_collision_hull->m_aabb.center.z - m_camera_position.z) < collision_distance &&
		wall_collision_hull->intersectRay(m_camera_position, m_camera_position + glm::vec3(0, 0, 1.f), m_world_matrix, isectT, primID)) {
		gameEnded = true;
		time_of_collision = m_continous_time;
	}

}

void Renderer::RenderDeferredShading()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_texture, 0);

	GLenum drawbuffers[1] = { GL_COLOR_ATTACHMENT0 };

	glDrawBuffers(1, drawbuffers);

	glViewport(0, 0, m_screen_width, m_screen_height);

	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	
	glClear(GL_COLOR_BUFFER_BIT);

	m_deferred_program.Bind();

	m_deferred_program.loadVec3("uniform_light_color", m_light.GetColor());
	m_deferred_program.loadVec3("uniform_light_dir", m_light.GetDirection());
	m_deferred_program.loadVec3("uniform_light_pos", m_light.GetPosition());

	m_deferred_program.loadFloat("uniform_light_umbra", m_light.GetUmbra());
	m_deferred_program.loadFloat("uniform_light_penumbra", m_light.GetPenumbra());

	m_deferred_program.loadVec3("uniform_camera_pos", m_camera_position);
	m_deferred_program.loadVec3("uniform_camera_dir", normalize(m_camera_target_position - m_camera_position));

	m_deferred_program.loadMat4("uniform_light_projection_view", m_light.GetProjectionMatrix() * m_light.GetViewMatrix());
	m_deferred_program.loadInt("uniform_cast_shadows", m_light.GetCastShadowsStatus() ? 1 : 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	m_deferred_program.loadInt("uniform_tex_pos", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	m_deferred_program.loadInt("uniform_tex_normal", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	m_deferred_program.loadInt("uniform_tex_albedo", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	m_deferred_program.loadInt("uniform_tex_mask", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	m_deferred_program.loadInt("uniform_tex_depth", 4);

	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D, m_light.GetShadowMapDepthTexture());
	m_deferred_program.loadInt("uniform_shadow_map", 10);

	glBindVertexArray(m_vao_fbo);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);

	m_deferred_program.Unbind();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDepthMask(GL_TRUE);
}

void Renderer::RenderGeometry()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_pos_texture, 0);

	GLenum drawbuffers[4] = {
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3 };

	glDrawBuffers(4, drawbuffers);

	glViewport(0, 0, m_screen_width, m_screen_height);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_geometry_program.Bind();
	RenderStaticGeometry();
	
	// No need to render the collision hulls
	//RenderCollidableGeometry();

	// Instead, just run collision checks
	CollisionCheck();

	m_geometry_program.Unbind();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
}

void Renderer::RenderShadowMaps()
{
	if (m_light.GetCastShadowsStatus())
	{
		int m_depth_texture_resolution = m_light.GetShadowMapResolution();

		glBindFramebuffer(GL_FRAMEBUFFER, m_light.GetShadowMapFBO());
		glViewport(0, 0, m_depth_texture_resolution, m_depth_texture_resolution);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);

		// Bind the shadow mapping program
		m_spot_light_shadow_map_program.Bind();

		glm::mat4 proj = m_light.GetProjectionMatrix() * m_light.GetViewMatrix() * m_world_matrix;

		RenderShadowMapsForAll(proj, m_nodes);
		RenderShadowMapsForAll(proj, m_corridor_nodes);

		glm::vec3 camera_dir = normalize(m_camera_target_position - m_camera_position);
		float_t isectT = 0.f;
		int32_t primID;

		/*
		for (auto& node : this->m_collidables_nodes)
		{
			//if (node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT, primID)) continue;
			node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT, primID);

			glBindVertexArray(node->m_vao);

			m_spot_light_shadow_map_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);

			for (int j = 0; j < node->parts.size(); ++j)
			{
				glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			}

			glBindVertexArray(0);
		}
		*/

		m_spot_light_shadow_map_program.Unbind();
		glDisable(GL_DEPTH_TEST);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void Renderer::RenderShadowMapsForAll(glm::mat4 proj, std::vector<GeometryNode*> nodes)
{
	for (auto& node : nodes)
	{
		glBindVertexArray(node->m_vao);

		m_spot_light_shadow_map_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);

		for (int j = 0; j < node->parts.size(); ++j)
		{
			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
		}

		glBindVertexArray(0);
	}
}

void Renderer::CameraMoveForward(bool enable)
{
	m_camera_movement.x = (enable) ? 1.f : 0.f;
}

void Renderer::InitiateJump()
{
	// If the previous jump/slide hasn't finished, then don't let the player jump
	if (InAir() || Sliding())
		return;

	time_of_jump = m_continous_time;
	point_of_jump = m_camera_position.z;
	std::cout << point_of_jump << std::endl;
}
bool Renderer::InAir() {
	if (time_of_jump == -1)
		return false;

	return (time_of_jump + expected_jump_duration > m_continous_time);
}

void Renderer::InitiateSlide()
{
	// If the previous jump/slide hasn't finished, then don't let the player slide
	if (InAir() || Sliding())
		return;

	time_of_slide = m_continous_time;
	point_of_slide = m_camera_position.z;
}
bool Renderer::Sliding() {
	if (time_of_slide == -1)
		return false;

	return (time_of_slide + expected_slide_duration > m_continous_time);
}

void Renderer::CameraMoveLeft(bool enable)
{
	m_camera_movement.y = (enable) ? -1.f : 0.f;
}
void Renderer::CameraMoveRight(bool enable)
{
	m_camera_movement.y = (enable) ? 1.f : 0.f;
}

void Renderer::CameraLook(glm::vec2 lookDir)
{
	m_camera_look_angle_destination = lookDir;
}

void Renderer::SetRunning(bool newValue) {
	currently_running = newValue;
}

void Renderer::restart() {
	this->InitCamera();
	this->BuildWorld();
	bool light_initialization = InitLights();

	time_of_jump = -1;
	time_of_slide = -1;

	point_of_jump = -1;
	point_of_slide = -1;

	for (auto& node : m_corridor_nodes) {
		node->app_model_matrix = node->model_matrix;
	}

	for (auto& node : m_wall_nodes) {
		node->app_model_matrix = node->model_matrix;
	}

	gameEnded = false;
}
