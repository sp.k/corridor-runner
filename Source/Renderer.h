#ifndef BIM_ENGINE_RENDERER_H
#define BIM_ENGINE_RENDERER_H

#include "GLEW\glew.h"
#include "glm\glm.hpp"
#include <vector>
#include "ShaderProgram.h"
#include "GeometryNode.h"
#include "CollidableNode.h"
#include "LightNode.h"

class Renderer
{
public:
	// Empty

protected:
	int												m_screen_width, m_screen_height;
	glm::mat4										m_world_matrix;
	glm::mat4										m_view_matrix;
	glm::mat4										m_projection_matrix;
	glm::vec3										m_camera_position;
	glm::vec3										m_camera_target_position;
	glm::vec3										m_camera_up_vector;
	glm::vec2										m_camera_movement;
	glm::vec2										m_camera_look_angle_destination;
	
	float m_continous_time;

	// Protected Functions
	bool InitShaders();
	bool InitGeometricMeshes();
	bool InitCommonItems();
	bool InitLights();
	bool InitIntermediateBuffers();
	void BuildWorld();
	void InitCamera();
	void RenderGeometry();
	void RenderDeferredShading();
	void RenderStaticGeometry();
	void RenderStaticGeometryForAll(glm::mat4 proj, std::vector<GeometryNode*> nodes);
	void RenderCollidableGeometry();
	void RenderCollidableGeometryForAll(glm::mat4 proj, std::vector<CollidableNode*> nodes);
	void RenderShadowMaps();
	void RenderShadowMapsForAll(glm::mat4 proj, std::vector<GeometryNode*> nodes);
	void RenderPostProcess();

	void CollisionCheck();

	enum OBJECTS
	{
		WALL = 0,
		CORRIDOR_STRAIGHT,
		
		CH_WALL,			// collidable objects last

		SIZE_ALL
	};

	std::vector<GeometryNode*> m_nodes;
	//std::vector<CollidableNode*> m_collidables_nodes;

	LightNode									m_light;
	ShaderProgram								m_geometry_program;
	ShaderProgram								m_deferred_program;
	ShaderProgram								m_post_program;
	ShaderProgram								m_spot_light_shadow_map_program;

	GLuint m_fbo;
	GLuint m_vao_fbo;
	GLuint m_vbo_fbo_vertices;

	GLuint m_fbo_texture;

	GLuint m_fbo_depth_texture;
	GLuint m_fbo_pos_texture;
	GLuint m_fbo_normal_texture;
	GLuint m_fbo_albedo_texture;
	GLuint m_fbo_mask_texture;


	//--- Custom fields ---------------------------------------------------------------
	bool gameEnded = false;
	float time_of_collision = -1.0f;

	//--- Corridors -------------------------------------------------------------------
	std::vector<GeometryNode*> m_corridor_nodes;
	float corridor_length = 20.4486008f;
	float corridor_height = 10.6452999f;
	float corridor_width =  10.6452999f;	// not really but ok
	const int CORRIDOR_COUNT = 7;

	//--- Walls -----------------------------------------------------------------------
	std::vector<GeometryNode*> m_wall_nodes;
	CollidableNode* wall_collision_hull;
	float default_wall_distance = corridor_length * 1.0f;
	float wall_thickness = 1.f;	// approximately (?)
	const int WALL_COUNT = 7;
	float clear_start_length = corridor_length;	// The distance at the begining of the game that has no obstacles

	float lateral_movement_limit = corridor_width / 2 - 2;	// =~ 3

	float wall_lateral_displacement_far = corridor_width / 2;	// ~= 5		so that the camera collides when on the side
	float wall_lateral_displacement_close = corridor_width / 4;	// ~= 2.5	so that the camera collides when EITHER on the side or center

	int player_height = 5;

	float floor_level = - corridor_height / 2;	// =~ -5.3

	float collision_distance = 3.f;

	//--- Jump ------------------------------------------------------------------------
	float g = 28.f;		// This is intentionally not on par with reality
	float time_of_jump = -1;
	float point_of_jump = -1;
	float height_increase_of_jump = player_height * 0.5f;
	float launch_speed_of_jump = sqrt(2 * g * height_increase_of_jump);
	float expected_jump_duration = 2 * launch_speed_of_jump / g;
	float jump_multiplier = 1.f;

	//--- Slide -----------------------------------------------------------------------
	float time_of_slide = -1;
	float point_of_slide = -1;
	float height_reduction_of_slide = player_height * 0.5f;

	//float original_max = ...;
	float first_root = 0.628717;
	// The function used for the slide is:			y = (2*x)^4 - (player_height - height_reduction_of_slide)
	// Shifted by its first_root:					y = (2*u)^4 - (player_height - height_reduction_of_slide)	 with u = x - first_root
	float expected_slide_duration = 1.257434;
	//float max_adjustment_factor = height_reduction_of_slide / original_max;
	float slide_multiplier = 1.f;

	//--- Running ---------------------------------------------------------------------
	bool currently_running = false;
	float running_speed_multiplier = 1.4f;




	float initial_light_intensity = 100.f;

	bool firstUpdate = true;


	glm::vec3 starting_position = glm::vec3(0.f, floor_level + player_height, 0.f);  // = (0, ~0, 0)
	glm::vec3 flashlight_offset = glm::vec3(-0.1f, -0.5f, 0.f);

public:

	Renderer();
	~Renderer();
	bool Init(int SCREEN_WIDTH, int SCREEN_HEIGHT);
	void Update(float dt);
	bool ResizeBuffers(int SCREEN_WIDTH, int SCREEN_HEIGHT);
	void UpdateGeometry(float dt);
	void UpdateCamera(float dt);
	bool ReloadShaders();
	void Render();
		
	void CameraMoveForward(bool enable);
	void CameraMoveLeft(bool enable);
	void CameraMoveRight(bool enable);
	void CameraLook(glm::vec2 lookDir);
		
	void InitiateJump();
	void InitiateSlide();
	bool InAir();
	bool Sliding();
	bool CanJump();
	bool CanSlide();
	void SetRunning(bool newValue);

	void restart();
};

#endif
